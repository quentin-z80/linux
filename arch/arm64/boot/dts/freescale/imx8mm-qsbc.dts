// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright 2023 Quentin Freimanis
 */

/dts-v1/;

#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/leds/common.h>
#include <dt-bindings/phy/phy-imx8-pcie.h>
#include "imx8mm.dtsi"

/ {
    model = "q-lab qsbc";
    compatible = "q-lab,qsbc", "fsl,imx8mm";

    aliases {
        spi0 = &flexspi;
        mmc0 = &usdhc3;
        mmc1 = &usdhc2;
    };

    chosen {
            stdout-path = &uart2;
    };

    memory@40000000 {
		device_type = "memory";
		reg = <0x0 0x40000000 0 0x80000000>;
	};

    leds {
        compatible = "gpio-leds";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_gpio_led>;

        led-0 {
            gpio = <&gpio2 2 GPIO_ACTIVE_LOW>;
			function = LED_FUNCTION_STATUS;
            linux,default-trigger = "heartbeat";
            color = <LED_COLOR_ID_GREEN>;
        };

        led-1 {
            gpio = <&gpio2 3 GPIO_ACTIVE_LOW>;
			function = LED_FUNCTION_STATUS;
            color = <LED_COLOR_ID_BLUE>;
        };
    };

	sysinfo {
		compatible = "gpio-sysinfo";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_gpio_sysinfo>;
		gpios = <&gpio3 22 GPIO_ACTIVE_HIGH>,
				<&gpio3 23 GPIO_ACTIVE_HIGH>,
				<&gpio3 24 GPIO_ACTIVE_HIGH>;
		revisions = <1>, <3>;
		names = "rev 1", "rev 1.1";
	};

    pcie0_refclk: pcie0-refclk {
		compatible = "fixed-clock";
		#clock-cells = <0>;
		clock-frequency = <100000000>;
	};

    dsi86_refclk: dsi86-refclk {
        compatible = "fixed-clock";
        #clock-cells = <0>;
        clock-frequency = <26000000>;
    };

    reg_usdhc2_vmmc: regulator-usdhc2 {
		compatible = "regulator-fixed";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_reg_usdhc2_vmmc>;
		regulator-name = "VDD_SD2";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		gpio = <&gpio2 19 GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};

    reg_dsi86_1v2: regulator-dsi86-1v2 {
        compatible = "regulator-fixed";
        regulator-name = "VDD_DSI86_1V2";
        regulator-min-microvolt = <1200000>;
        regulator-max-microvolt = <1200000>;
    };

    reg_usb1_vbus: reg-usb1-vbus {
        compatible = "regulator-fixed";
        pinctrl-names = "default";
        pinctrl-0 = <&pinctrl_reg_usb1_vbus>;
        regulator-name = "USB1_VBUS";
        regulator-min-microvolt = <5000000>;
        regulator-max-microvolt = <5000000>;
        gpio = <&gpio1 15 GPIO_ACTIVE_HIGH>;
        enable-active-high;
    };

    reg_usb2_vbus: reg-usb2-vbus {
        compatible = "regulator-fixed";
        pinctrl-names = "default";
        pinctrl-0 = <&pinctrl_reg_usb2_vbus>;
        regulator-name = "USB2_VBUS";
        regulator-min-microvolt = <5000000>;
        regulator-max-microvolt = <5000000>;
        gpio = <&gpio1 12 GPIO_ACTIVE_HIGH>;
        enable-active-high;
    };

/*
    connector {
        compatible = "dp-connector";
        label = "dp0";
        type = "full-size";

        port {
            dp_connector_in: endpoint {
                remote-endpoint = <&dp_out>;
            };
        };
    };
*/

};

&snvs_pwrkey {
	status = "okay";
};

&A53_0 {
	cpu-supply = <&buck2_reg>;
};

&A53_1 {
	cpu-supply = <&buck2_reg>;
};

&A53_2 {
	cpu-supply = <&buck2_reg>;
};

&A53_3 {
	cpu-supply = <&buck2_reg>;
};

&ddrc {
	operating-points-v2 = <&ddrc_opp_table>;

	ddrc_opp_table: opp-table {
		compatible = "operating-points-v2";

		opp-25M {
			opp-hz = /bits/ 64 <25000000>;
		};

		opp-100M {
			opp-hz = /bits/ 64 <100000000>;
		};

		opp-750M {
			opp-hz = /bits/ 64 <750000000>;
		};
	};
};

&fec1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_fec1>;
	phy-mode = "rgmii-id";
	phy-handle = <&ethphy0>;
	status = "okay";

	mdio {
		#address-cells = <1>;
		#size-cells = <0>;

		ethphy0: ethernet-phy@1 {
			compatible = "ethernet-phy-ieee802.3-c22";
			reg = <1>;
            interrupt-parent = <&gpio1>;
            interrupts = <0 IRQ_TYPE_LEVEL_LOW>;
			reset-gpios = <&gpio1 1 GPIO_ACTIVE_LOW>;
			reset-assert-us = <10000>;
			reset-deassert-us = <80000>;
			realtek,clkout-disable;
			vddio-supply = <&vddio>;

			vddio: vddio-regulator {
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
			};
		};
	};
};

&i2c1 {
	clock-frequency = <400000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_i2c1>;
	status = "okay";

	pmic@4b {
		compatible = "rohm,bd71847";
		reg = <0x4b>;
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_pmic>;
		interrupt-parent = <&gpio1>;
		interrupts = <4 IRQ_TYPE_LEVEL_LOW>;
		rohm,reset-snvs-powered;

		#clock-cells = <0>;
		clocks = <&osc_32k 0>;
		clock-output-names = "clk-32k-out";

		regulators {
			buck1_reg: BUCK1 {
				regulator-name = "buck1";
				regulator-min-microvolt = <700000>;
				regulator-max-microvolt = <1300000>;
				regulator-boot-on;
				regulator-always-on;
				regulator-ramp-delay = <1250>;
			};

			buck2_reg: BUCK2 {
				regulator-name = "buck2";
				regulator-min-microvolt = <700000>;
				regulator-max-microvolt = <1300000>;
				regulator-boot-on;
				regulator-always-on;
				regulator-ramp-delay = <1250>;
				rohm,dvs-run-voltage = <1000000>;
				rohm,dvs-idle-voltage = <900000>;
			};

			buck3_reg: BUCK3 {
				// BUCK5 in datasheet
				regulator-name = "buck3";
				regulator-min-microvolt = <700000>;
				regulator-max-microvolt = <1350000>;
				regulator-boot-on;
				regulator-always-on;
			};

			buck4_reg: BUCK4 {
				// BUCK6 in datasheet
				regulator-name = "buck4";
				regulator-min-microvolt = <3000000>;
				regulator-max-microvolt = <3300000>;
				regulator-boot-on;
				regulator-always-on;
			};

			buck5_reg: BUCK5 {
				// BUCK7 in datasheet
				regulator-name = "buck5";
				regulator-min-microvolt = <1605000>;
				regulator-max-microvolt = <1995000>;
				regulator-boot-on;
				regulator-always-on;
			};

			buck6_reg: BUCK6 {
				// BUCK8 in datasheet
				regulator-name = "buck6";
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <1400000>;
				regulator-boot-on;
				regulator-always-on;
			};

			ldo1_reg: LDO1 {
				regulator-name = "ldo1";
				regulator-min-microvolt = <1600000>;
				regulator-max-microvolt = <3300000>;
				regulator-boot-on;
				regulator-always-on;
			};

			ldo2_reg: LDO2 {
				regulator-name = "ldo2";
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <900000>;
				regulator-boot-on;
				regulator-always-on;
			};

			ldo3_reg: LDO3 {
				regulator-name = "ldo3";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <3300000>;
				regulator-boot-on;
				regulator-always-on;
			};

			ldo4_reg: LDO4 {
				regulator-name = "ldo4";
				regulator-min-microvolt = <900000>;
				regulator-max-microvolt = <1800000>;
				regulator-boot-on;
				regulator-always-on;
			};

			ldo6_reg: LDO6 {
				regulator-name = "ldo6";
				regulator-min-microvolt = <900000>;
				regulator-max-microvolt = <1800000>;
				regulator-boot-on;
				regulator-always-on;
			};
		};
	};

/*
    edp_bridge: bridge@2c {
		compatible = "ti,sn65dsi86";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_edp_bridge>;
		reg = <0x2c>;
		enable-gpios = <&gpio5 10 GPIO_ACTIVE_LOW>;
		interrupt-parent = <&gpio5>;
		interrupts = <7 IRQ_TYPE_LEVEL_HIGH>;
		vccio-supply = <&buck5_reg>;
		vpll-supply = <&buck5_reg>;
		vcca-supply = <&reg_dsi86_1v2>;
		vcc-supply = <&reg_dsi86_1v2>;

        clock-names = "dsi86_refclk";
        clocks = <&dsi86_refclk>;

		ports {
			#address-cells = <1>;
			#size-cells = <0>;

			port@0 {
				reg = <0>;

				edp_bridge_in: endpoint {
					remote-endpoint = <&mipi_dsi_out>;
				};
			};

			port@1 {
				reg = <1>;

				edp_bridge_out: endpoint {
                    data-lanes = <3 2 1 0>;
					remote-endpoint = <&dp_out>;
				};
			};
		};
	};
*/
};

&i2c2 {
	clock-frequency = <400000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_i2c2>;
	status = "okay";

};

&i2c4 {
    clock-frequency = <400000>;
    pinctrl-names = "default";
    pinctrl-0 = <&pinctrl_i2c4>;
    status = "okay";

    tusb1: tusb320@67 {
        compatible = "ti,tusb320l";
        pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_tusb1>;
        reg = <0x67>;
        interrupt-parent = <&gpio1>;
        interrupts = <14 IRQ_TYPE_LEVEL_LOW>;

        typec1: connector {
            compatable = "usb-c-connector";
            label = "usb1";
            power-role = "dual";
			try-power-role = "source";
            data-role = "dual";
			try-data-role = "source";
            typec-power-opmode = "1.5A";
			pd-disable;

			ports {
                #address-cells = <1>;
                #size-cells = <0>;

                port@0 {
                    reg = <0>;
                    usb1hs_ep: endpoint {
                        remote-endpoint = <&usb1hs>;
                    };
                };
            };
        };
    };

    tusb2: tusb320@47 {
        compatible = "ti,tusb320l";
        pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_tusb2>;
        reg = <0x47>;
        interrupt-parent = <&gpio1>;
        interrupts = <11 IRQ_TYPE_LEVEL_LOW>;

        typec2: connector {
            compatable = "usb-c-connector";
            label = "usb2";
            power-role = "dual";
			try-power-role = "source";
            data-role = "dual";
			try-data-role = "source";
            typec-power-opmode = "1.5A";
			pd-disable;

			ports {
                #address-cells = <1>;
                #size-cells = <0>;

                port@0 {
                    reg = <0>;
                    usb2hs_ep: endpoint {
                        remote-endpoint = <&usb2hs>;
                    };
                };
            };
        };
    };

    power_sensor: ina231@40 {
		compatible = "ti,ina231";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_ina231>;
		interrupt-parent = <&gpio1>;
		interrupts = <3 IRQ_TYPE_LEVEL_LOW>;
		reg = <0x40>;
		shunt-resistor = <10000>;
	};
};

&usbotg1 {
    dr_mode = "otg";
	disable-over-current;
	vbus-supply = <&reg_usb1_vbus>;
    status = "okay";

    port {
        usb1hs: endpoint {
            remote-endpoint = <&usb1hs_ep>;
        };
    };

};

&usbotg2 {
    dr_mode = "otg";
	disable-over-current;
	vbus-supply = <&reg_usb2_vbus>;
    status = "okay";

    port {
        usb2hs: endpoint {
            remote-endpoint = <&usb2hs_ep>;
        };
    };
};

/*
&mipi_dsi {
	status = "okay";

	ports {
		port@1 {
			reg = <1>;

			mipi_dsi_out: endpoint {
				remote-endpoint = <&edp_bridge_in>;
			};
		};
	};
};
*/

&pcie_phy {
	fsl,clkreq-unsupported;
	fsl,refclk-pad-mode = <IMX8_PCIE_REFCLK_PAD_INPUT>;
	clocks = <&pcie0_refclk>;
	status = "okay";
};

&pcie0 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_pcie0>;
	reset-gpio = <&gpio5 18 GPIO_ACTIVE_LOW>;
	clocks = <&clk IMX8MM_CLK_PCIE1_ROOT>, <&clk IMX8MM_CLK_PCIE1_AUX>,
		     <&pcie0_refclk>;
	clock-names = "pcie", "pcie_aux", "pcie_bus";
	assigned-clocks = <&clk IMX8MM_CLK_PCIE1_AUX>,
			          <&clk IMX8MM_CLK_PCIE1_CTRL>;
	assigned-clock-rates = <10000000>, <250000000>;
	assigned-clock-parents = <&clk IMX8MM_SYS_PLL2_50M>,
	                         <&clk IMX8MM_SYS_PLL2_250M>;
	vpcie-supply = <&buck4_reg>;
	status = "okay";
};

&uart1 {
    pinctrl-names = "default";
    pinctrl-0 = <&pinctrl_uart1>;
    status = "okay";
};

&uart2 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart2>;
	status = "okay";
};

&flexspi {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_flexspi>;
	status = "okay";

	flash@0 {
		reg = <0>;
		#address-cells = <1>;
		#size-cells = <1>;
		compatible = "jedec,spi-nor";
		spi-max-frequency = <80000000>;
		spi-tx-bus-width = <1>;
		spi-rx-bus-width = <4>;
	};
};

&usdhc2 {
	assigned-clocks = <&clk IMX8MM_CLK_USDHC2>;
	assigned-clock-rates = <200000000>;
	pinctrl-names = "default", "state_100mhz", "state_200mhz";
	pinctrl-0 = <&pinctrl_usdhc2>, <&pinctrl_usdhc2_gpio>;
	pinctrl-1 = <&pinctrl_usdhc2_100mhz>, <&pinctrl_usdhc2_gpio>;
	pinctrl-2 = <&pinctrl_usdhc2_200mhz>, <&pinctrl_usdhc2_gpio>;
	cd-gpios = <&gpio2 12 GPIO_ACTIVE_LOW>;
	bus-width = <4>;
	vmmc-supply = <&reg_usdhc2_vmmc>;
	status = "okay";
};

&usdhc3 {
	assigned-clocks = <&clk IMX8MM_CLK_USDHC3_ROOT>;
	assigned-clock-rates = <400000000>;
    pinctrl-names = "default", "state_100mhz", "state_200mhz";
	pinctrl-0 = <&pinctrl_usdhc3>, <&pinctrl_usdhc3_gpio>;
	pinctrl-1 = <&pinctrl_usdhc3_100mhz>;
	pinctrl-2 = <&pinctrl_usdhc3_200mhz>;
	reset-gpios = <&gpio3 16 GPIO_ACTIVE_LOW>;
	bus-width = <8>;
	non-removable;
	status = "okay";
};

&wdog1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_wdog>;
	fsl,ext-reset-output;
	status = "okay";
};

&iomuxc {
	pinctrl_gpio_led: gpiogledgrp {
		fsl,pins = <
			MX8MM_IOMUXC_SD1_DATA0_GPIO2_IO2	0x140
			MX8MM_IOMUXC_SD1_DATA1_GPIO2_IO3	0x140
		>;
	};

	pinctrl_gpio_sysinfo: gpiosysinfogrp {
		fsl,pins = <
			MX8MM_IOMUXC_SAI5_RXD1_GPIO3_IO22	0x100
			MX8MM_IOMUXC_SAI5_RXD2_GPIO3_IO23	0x100
			MX8MM_IOMUXC_SAI5_RXD3_GPIO3_IO24	0x100
		>;
	};

	pinctrl_reg_usdhc2_vmmc: usdhc2_reggrp {
		fsl,pins = <
			MX8MM_IOMUXC_SD1_RESET_B_GPIO2_IO10 0x0
		>;
	};

	pinctrl_reg_usb1_vbus: usb1_reggrp {
		fsl,pins = <
			MX8MM_IOMUXC_GPIO1_IO15_GPIO1_IO15 0x0
		>;
	};

	pinctrl_reg_usb2_vbus: usb2_reggrp {
		fsl,pins = <
			MX8MM_IOMUXC_GPIO1_IO12_GPIO1_IO12 0x0
		>;
	};

	pinctrl_fec1: fec1grp {
		fsl,pins = <
			MX8MM_IOMUXC_ENET_MDC_ENET1_MDC				0x2
			MX8MM_IOMUXC_ENET_MDIO_ENET1_MDIO			0x2
			MX8MM_IOMUXC_ENET_TD3_ENET1_RGMII_TD3		0x16
			MX8MM_IOMUXC_ENET_TD2_ENET1_RGMII_TD2		0x16
			MX8MM_IOMUXC_ENET_TD1_ENET1_RGMII_TD1		0x16
			MX8MM_IOMUXC_ENET_TD0_ENET1_RGMII_TD0		0x16
			MX8MM_IOMUXC_ENET_RD3_ENET1_RGMII_RD3		0x90
			MX8MM_IOMUXC_ENET_RD2_ENET1_RGMII_RD2		0x90
			MX8MM_IOMUXC_ENET_RD1_ENET1_RGMII_RD1		0x90
			MX8MM_IOMUXC_ENET_RD0_ENET1_RGMII_RD0		0x90
			MX8MM_IOMUXC_ENET_TXC_ENET1_RGMII_TXC		0x16
			MX8MM_IOMUXC_ENET_RXC_ENET1_RGMII_RXC		0x90
			MX8MM_IOMUXC_ENET_RX_CTL_ENET1_RGMII_RX_CTL	0x90
			MX8MM_IOMUXC_ENET_TX_CTL_ENET1_RGMII_TX_CTL	0x16
			MX8MM_IOMUXC_GPIO1_IO00_GPIO1_IO0			0x90
			MX8MM_IOMUXC_GPIO1_IO01_GPIO1_IO1			0x10
		>;
	};

	pinctrl_i2c1: i2c1grp {
		fsl,pins = <
			MX8MM_IOMUXC_I2C1_SCL_I2C1_SCL	0x40000082
			MX8MM_IOMUXC_I2C1_SDA_I2C1_SDA	0x40000082
		>;
	};

	pinctrl_pmic: pmicgrp {
		fsl,pins = <
			MX8MM_IOMUXC_GPIO1_IO04_GPIO1_IO4	0x140
		>;
	};

	pinctrl_edp_bridge: edpbridgegrp {
		fsl,pins = <
			MX8MM_IOMUXC_ECSPI2_SCLK_GPIO5_IO10 0x0
			MX8MM_IOMUXC_ECSPI1_MOSI_GPIO5_IO7 0x100
		>;
	};

	pinctrl_i2c2: i2c2grp {
		fsl,pins = <
			MX8MM_IOMUXC_I2C2_SCL_I2C2_SCL	0x40000082
			MX8MM_IOMUXC_I2C2_SDA_I2C2_SDA	0x40000082
		>;
	};

	pinctrl_i2c4: i2c4grp {
		fsl,pins = <
			MX8MM_IOMUXC_I2C4_SCL_I2C4_SCL	0x40000082
			MX8MM_IOMUXC_I2C4_SDA_I2C4_SDA	0x40000082
		>;
	};

	pinctrl_tusb1: tusb1grp {
		fsl,pins = <
			MX8MM_IOMUXC_GPIO1_IO14_GPIO1_IO14	0x80
		>;
	};

	pinctrl_tusb2: tusb2grp {
		fsl,pins = <
			MX8MM_IOMUXC_GPIO1_IO11_GPIO1_IO11	0x80
		>;
	};

	pinctrl_ina231: ina231grp {
		fsl,pins = <
			MX8MM_IOMUXC_GPIO1_IO03_GPIO1_IO3	0x80
		>;
	};

	pinctrl_pcie0: pcie0grp {
		fsl,pins = <
			MX8MM_IOMUXC_I2C3_SCL_GPIO5_IO18	0x0
			MX8MM_IOMUXC_ECSPI2_MOSI_GPIO5_IO11 0x0
		>;
	};

	pinctrl_uart1: uart1grp {
		fsl,pins = <
			MX8MM_IOMUXC_SAI2_RXFS_UART1_DCE_TX		0x140
			MX8MM_IOMUXC_SAI2_RXC_UART1_DCE_RX		0x140
			MX8MM_IOMUXC_SAI2_TXFS_UART1_DCE_CTS_B	0x140
			MX8MM_IOMUXC_SAI2_RXD0_UART1_DCE_RTS_B	0x140
		>;
	};

	pinctrl_uart2: uart2grp {
		fsl,pins = <
			MX8MM_IOMUXC_UART2_TXD_UART2_DCE_TX 	0x140
			MX8MM_IOMUXC_UART2_RXD_UART2_DCE_RX 	0x140
			MX8MM_IOMUXC_SAI3_RXC_UART2_DCE_CTS_B 	0x140
			MX8MM_IOMUXC_SAI3_RXD_UART2_DCE_RTS_B 	0x140
		>;
	};

	pinctrl_flexspi: flexspigrp {
		fsl,pins = <
			MX8MM_IOMUXC_NAND_ALE_QSPI_A_SCLK		0x1c2
			MX8MM_IOMUXC_NAND_CE0_B_QSPI_A_SS0_B	0x82
			MX8MM_IOMUXC_NAND_DATA00_QSPI_A_DATA0	0x82
			MX8MM_IOMUXC_NAND_DATA01_QSPI_A_DATA1	0x82
			MX8MM_IOMUXC_NAND_DATA02_QSPI_A_DATA2	0x82
			MX8MM_IOMUXC_NAND_DATA03_QSPI_A_DATA3	0x82
		>;
	};

	pinctrl_usdhc2_gpio: usdhc2gpiogrp {
		fsl,pins = <
			MX8MM_IOMUXC_SD2_CD_B_GPIO2_IO12		0x1c4
		>;
	};

	pinctrl_usdhc2: usdhc2grp {
		fsl,pins = <
			MX8MM_IOMUXC_SD2_CLK_USDHC2_CLK			0x190
			MX8MM_IOMUXC_SD2_CMD_USDHC2_CMD			0x1d0
			MX8MM_IOMUXC_SD2_DATA0_USDHC2_DATA0		0x1d0
			MX8MM_IOMUXC_SD2_DATA1_USDHC2_DATA1		0x1d0
			MX8MM_IOMUXC_SD2_DATA2_USDHC2_DATA2		0x1d0
			MX8MM_IOMUXC_SD2_DATA3_USDHC2_DATA3		0x1d0
		>;
	};

	pinctrl_usdhc2_100mhz: usdhc2_100mhzgrp {
		fsl,pins = <
			MX8MM_IOMUXC_SD2_CLK_USDHC2_CLK			0x194
			MX8MM_IOMUXC_SD2_CMD_USDHC2_CMD			0x1d4
			MX8MM_IOMUXC_SD2_DATA0_USDHC2_DATA0		0x1d4
			MX8MM_IOMUXC_SD2_DATA1_USDHC2_DATA1		0x1d4
			MX8MM_IOMUXC_SD2_DATA2_USDHC2_DATA2		0x1d4
			MX8MM_IOMUXC_SD2_DATA3_USDHC2_DATA3		0x1d4
		>;
	};

	pinctrl_usdhc2_200mhz: usdhc2_200mhzgrp {
		fsl,pins = <
			MX8MM_IOMUXC_SD2_CLK_USDHC2_CLK			0x196
			MX8MM_IOMUXC_SD2_CMD_USDHC2_CMD			0x1d6
			MX8MM_IOMUXC_SD2_DATA0_USDHC2_DATA0		0x1d6
			MX8MM_IOMUXC_SD2_DATA1_USDHC2_DATA1		0x1d6
			MX8MM_IOMUXC_SD2_DATA2_USDHC2_DATA2		0x1d6
			MX8MM_IOMUXC_SD2_DATA3_USDHC2_DATA3		0x1d6
		>;
	};

	pinctrl_usdhc3: usdhc3grp {
		fsl,pins = <
			MX8MM_IOMUXC_NAND_WE_B_USDHC3_CLK		0x190
			MX8MM_IOMUXC_NAND_WP_B_USDHC3_CMD		0x1d0
			MX8MM_IOMUXC_NAND_CE1_B_USDHC3_STROBE	0x190
			MX8MM_IOMUXC_NAND_DATA04_USDHC3_DATA0	0x1d0
			MX8MM_IOMUXC_NAND_DATA05_USDHC3_DATA1	0x1d0
			MX8MM_IOMUXC_NAND_DATA06_USDHC3_DATA2	0x1d0
			MX8MM_IOMUXC_NAND_DATA07_USDHC3_DATA3	0x1d0
			MX8MM_IOMUXC_NAND_RE_B_USDHC3_DATA4		0x1d0
			MX8MM_IOMUXC_NAND_CE2_B_USDHC3_DATA5	0x1d0
			MX8MM_IOMUXC_NAND_CE3_B_USDHC3_DATA6	0x1d0
			MX8MM_IOMUXC_NAND_CLE_USDHC3_DATA7		0x1d0
		>;
	};

	pinctrl_usdhc3_gpio: usdhc3gpiogrp {
		fsl,pins = <
			MX8MM_IOMUXC_NAND_READY_B_GPIO3_IO16	0x1c4
		>;
	};

	pinctrl_usdhc3_100mhz: usdhc3_100mhzgrp {
		fsl,pins = <
			MX8MM_IOMUXC_NAND_WE_B_USDHC3_CLK		0x194
			MX8MM_IOMUXC_NAND_WP_B_USDHC3_CMD		0x1d4
			MX8MM_IOMUXC_NAND_CE1_B_USDHC3_STROBE	0x194
			MX8MM_IOMUXC_NAND_DATA04_USDHC3_DATA0	0x1d4
			MX8MM_IOMUXC_NAND_DATA05_USDHC3_DATA1	0x1d4
			MX8MM_IOMUXC_NAND_DATA06_USDHC3_DATA2	0x1d4
			MX8MM_IOMUXC_NAND_DATA07_USDHC3_DATA3	0x1d4
			MX8MM_IOMUXC_NAND_RE_B_USDHC3_DATA4		0x1d4
			MX8MM_IOMUXC_NAND_CE2_B_USDHC3_DATA5	0x1d4
			MX8MM_IOMUXC_NAND_CE3_B_USDHC3_DATA6	0x1d4
			MX8MM_IOMUXC_NAND_CLE_USDHC3_DATA7		0x1d4
		>;
	};

	pinctrl_usdhc3_200mhz: usdhc3_200mhzgrp {
		fsl,pins = <
			MX8MM_IOMUXC_NAND_WE_B_USDHC3_CLK		0x196
			MX8MM_IOMUXC_NAND_WP_B_USDHC3_CMD		0x1d6
			MX8MM_IOMUXC_NAND_CE1_B_USDHC3_STROBE	0x196
			MX8MM_IOMUXC_NAND_DATA04_USDHC3_DATA0	0x1d6
			MX8MM_IOMUXC_NAND_DATA05_USDHC3_DATA1	0x1d6
			MX8MM_IOMUXC_NAND_DATA06_USDHC3_DATA2	0x1d6
			MX8MM_IOMUXC_NAND_DATA07_USDHC3_DATA3	0x1d6
			MX8MM_IOMUXC_NAND_RE_B_USDHC3_DATA4		0x1d6
			MX8MM_IOMUXC_NAND_CE2_B_USDHC3_DATA5	0x1d6
			MX8MM_IOMUXC_NAND_CE3_B_USDHC3_DATA6	0x1d6
			MX8MM_IOMUXC_NAND_CLE_USDHC3_DATA7		0x1d6
		>;
	};

	pinctrl_wdog: wdoggrp {
		fsl,pins = <
			MX8MM_IOMUXC_GPIO1_IO02_WDOG1_WDOG_B	0x166
		>;
	};
};
